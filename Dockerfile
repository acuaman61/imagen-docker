FROM ubuntu
RUN apt-get update
RUN apt-get install -y python
RUN echo 1.0 >> /etc/version && apt-get install -y git \
	&& apt-get install -y iputils-ping

#Comando CMD se utiliza para ejecutar shell o comandos de arranque

#Se ejecuta siempre el último. ejecuta un shell

#CMD echo "Bienvenido a este contenedor"

#Formato JSON. lo ejecuta con exec
#CMD ["echo","Bienvenido a este contenedor"]

#Inicia el contenedor en bash
#CMD ["/bin/bash"]

#siempre ejecuta el comando

#WORKDIR Directorio de trabajo
RUN mkdir /datos
WORKDIR /datos
RUN touch f1.txt

##COPY##
#Copia archivos desde la ruta del contexto donde se tiene el arhivo Dockerfile a una ruta del contenedor
COPY index.html .

##ADD##
#tambien sirve para copiar archivos desde la ruta de contexto donde se tiene el archivo Dockerfile
ADD docs docs

##ENV##
#Definir variables de entorno
#ENV dir=/data dir1=data1
#RUN mkdir $dir && mkdir $dir1

##ARG##
#similar a ENV
#permite crear variables y enviar valores a la variable en el momento de crear la imagen
# ejemplo: docker build -t imagen:v1 --build-arg dir2=/data2 .

#ARG dir2
#RUN mkdir $dir2

##ejemplo 2 ARG
#crear un usuario 
#construir la imagen
#docker build -t imagen:v1 --build-arg dir2=/data2 --build-arg user=tom .
#ARG user
#ENV user_docker $user
#ADD add_user.sh /datos
#RUN /datos/add_user.sh

##EXPOSE##
#Exponer puertos
#EJEMPLO:instalar apache y subir servicio cuando arracnque la imagen
#RUN apt-get install -y apache2
#EXPOSE 80

#script para subir servicio apache
#ADD entrypoint.sh /datos

##CM##
#CMD /datos/entrypoint.sh


#se pueden adicionar argumentos y lo concatena al comando
ENTRYPOINT ["/bin/bash"]
